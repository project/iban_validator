INTRODUCTION
------------

This module, based on PHP-IBAN library, implements a validation plugin for Field
validation module.


REQUIREMENTS
------------

 * [Field validation](https://www.drupal.org/project/field_validation)
 * [PHP-IBAN](https://github.com/globalcitizen/php-iban) (external library)


USAGE EXAMPLE
-------------

1. Open the "Structure" administration page;
2. Go to "Content types" section and "Manage fields" to extend your entity
   (node, user, etc.);
3. Create a plain text field to store IBAN;
4. Go to "Field validation" section and add and configure a validation rule by
   selecting "IBAN" as validator.


CREDITS
-------

 * [Howard Ge](https://www.drupal.org/user/174740) (Field validation module
   author)
 * [Walter Stanish](https://github.com/globalcitizen) (PHP-IBAN library author)
